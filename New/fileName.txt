/**
	 * This method delete attachment for entity based on given projectID,
	 * extConfiguration,requestString, and entityID,
	 * 
	 * @return true if comment added
	 * @throws Exception
	 */
	public Boolean deleteAttachment(String projectId, ExtConfiguration extConfiguration, String entityID,
			String requestString) throws Exception {

		QmetryRestWrapper qmRestWrapper = new QmetryRestWrapper();
		String response = null;

		if (extConfiguration != null && projectId != null && entityID != null && requestString != null) {

			Map<String, String> requestHeaders = getHeaders(extConfiguration.getApikey(), true);

			StringBuilder uri = new StringBuilder(32);
			uri.append(extConfiguration.getBaseUrl());
			uri.append(AZUREUPDATEENTITY);

			String deleteAttachmentURI = uri.toString().replace("{entityID}", entityID);

			response = qmRestWrapper.doRequest(deleteAttachmentURI, HttpMethod.PATCH, requestHeaders, requestString);

			if ((qmRestWrapper.getResponseCode() == 200 || qmRestWrapper.getResponseCode() == 201)) {
				return true;
			}
		} else {
			throw new QmetryValidationException(MessageCodeBean.INTEGRATION.INVALID_EXT_CONFIGURATION_ID,
					EntityAbbrType.CO);
		}
		return false;
	}
